Search
******

Django application for searching

.. note:: The ``contact`` app is only included for testing purposes.

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-search
  # or
  python3 -m venv venv-search
  source venv-search/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

If you **do not** have Elasticsearch installed::

  pytest -x -m "not elasticsearch"

.. note:: We should add Elasticsearch to our GitLab testing configuration, so
          we don't have to include / exclude Elasticsearch tests:
          https://www.docker.elastic.co/

Usage
=====

::

  py.test -x && \
      touch temp.db && rm temp.db && \
      django-admin.py syncdb --noinput && \
      django-admin.py demo_data_login && \
      django-admin.py demo_data_search && \
      django-admin.py runserver

Release
=======

https://www.kbsoftware.co.uk/docs/
