# -*- encoding: utf-8 -*-
from django.views.generic import FormView, ListView

from base.view_utils import BaseMixin, ElasticPaginator, RedirectNextMixin
from search.forms import SearchForm
from search.models import SearchFormat
from search.search import SearchError, SearchIndex


class SearchViewMixin(BaseMixin, RedirectNextMixin, FormView, ListView):
    """

    .. note:: Do not set ``paginate_by`` because the ``ListView`` will create a
              Django paginator which does not work with ElasticSearch.

    """

    data_format = None
    form_class = SearchForm
    paginate_by_for_search = 20
    template_name = "search/search.html"

    def _criteria(self):
        return self.request.GET.get("criteria") or self.request.GET.get("q")

    def _data_format(self):
        return self.data_format or SearchFormat.COLUMN

    def _include_deleted(self):
        include_deleted = self.request.GET.get("include_deleted", "off")
        return "on" in include_deleted

    def _index_name(self):
        result = self.request.GET.get("index")
        if not result:
            if len(self.INDEX_CHOICES) == 1:
                result, _ = self.INDEX_CHOICES[0]
        return result

    def _page_number(self):
        page_number = 1
        if self.page_kwarg in self.request.GET:
            try:
                page_number = int(self.request.GET.get(self.page_kwarg))
            except ValueError:
                pass
        return page_number

    def _show_include_deleted(self):
        """Should the form display the 'include_deleted' field?

        .. note:: This doesn't make too much sense if some indexes support soft
                  delete and some don't.

        """
        result = False
        for k, v in self.INDEX_CLASSES.items():
            if v().is_soft_delete:
                result = True
                break
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        index_name = self._index_name()
        table_headings = []
        url_name = "."
        if index_name:
            table_headings = self.table_headings(index_name)
            url_name = self.url_name(index_name)
        page_number = self._page_number()
        page_obj = ElasticPaginator(
            page_number, self.get_paginate_by_for_search(), self.total
        )
        context.update(
            dict(
                criteria=self._criteria(),
                index_name=index_name,
                is_paginated=page_obj.has_other_pages(),
                page=page_obj,
                table_headings=table_headings,
                url_name=url_name,
            )
        )
        return context

    def get_form_kwargs(self):
        result = super().get_form_kwargs()
        result.update(
            dict(
                show_include_deleted=self._show_include_deleted(),
                index_choices=self.INDEX_CHOICES,
            )
        )
        return result

    def get_initial(self):
        result = super().get_initial()
        result.update(
            dict(
                criteria=self._criteria(),
                include_deleted=self._include_deleted(),
                index=self._index_name(),
            )
        )
        return result

    def get_paginate_by_for_search(self):
        """Get the number of items to paginate by, or ``20`` as a default."""
        return self.paginate_by_for_search or 20

    def get_queryset(self):
        result = []
        criteria = self._criteria()
        include_deleted = self._include_deleted()
        index_name = self._index_name()
        self.total = 0
        if index_name and criteria:
            result, self.total = self.search(
                index_name, criteria, include_deleted
            )
        return result

    def search(self, index_name, criteria, include_deleted):
        result = []
        page_number = self._page_number()
        search_class = self.search_class(index_name)
        if search_class:
            search_index = SearchIndex(search_class())
            result = search_index.search(
                criteria,
                data_format=self._data_format(),
                include_deleted=include_deleted,
                page_number=page_number,
                page_size=self.get_paginate_by_for_search(),
            )
        return result

    def search_class(self, index_name):
        result = self.INDEX_CLASSES.get(index_name)
        if not result:
            raise SearchError(
                "Cannot find an index class for '{}'".format(index_name)
            )
        return result

    def table_headings(self, index_name):
        result = []
        search_class = self.search_class(index_name)
        if search_class:
            result = search_class().table_headings()
        return result

    def url_name(self, index_name):
        result = []
        search_class = self.search_class(index_name)
        if search_class:
            result = search_class().url_name()
        return result
