# -*- encoding: utf-8 -*-
from django import forms


class SearchForm(forms.Form):

    index = forms.ChoiceField(label="")
    criteria = forms.CharField(max_length=100, label="", required=True)
    include_deleted = forms.BooleanField(required=False)

    def __init__(self, *args, **kwargs):
        index_choices = kwargs.pop("index_choices")
        show_include_deleted = kwargs.pop("show_include_deleted")
        super().__init__(*args, **kwargs)
        f = self.fields["index"]
        f.choices = index_choices
        for name in ("criteria", "index"):
            f = self.fields[name]
            f.widget.attrs.update({"class": "chosen-select pure-input-1"})
        if len(index_choices) == 1:
            del self.fields["index"]
        if not show_include_deleted:
            del self.fields["include_deleted"]
