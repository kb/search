# -*- encoding: utf-8 -*-
import attr
import json
import logging

from django.conf import settings
from django.utils import timezone
from elasticsearch import Elasticsearch, NotFoundError
from elasticsearch.helpers import streaming_bulk

from .models import SearchFormat


logger = logging.getLogger(__name__)


@attr.s
class Fmt:
    text = attr.ib()
    small = attr.ib(default=False)
    url = attr.ib(default=False)


@attr.s
class Hit:
    pk = attr.ib()
    document_type = attr.ib()
    score = attr.ib()
    is_deleted = attr.ib()
    data = attr.ib()


class SearchError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class SearchIndex:

    MAX_RESULT_WINDOW = 10000
    PAGE_SIZE = 20

    def __init__(self, search_class):
        self.search_class = search_class

    def _check_configuration(self):
        """For soft delete indexes, check 'mappings' include 'is_deleted'."""
        if self.search_class.is_soft_delete:
            document_type = properties = is_deleted = None
            mappings = self.search_class.configuration.get("mappings")
            if mappings:
                document_type = mappings.get(self.search_class.document_type)
            if document_type:
                properties = document_type.get("properties")
            if properties:
                is_deleted = properties.get("is_deleted")
            if not is_deleted:
                raise SearchError(
                    "The 'configuration' for a soft delete "
                    "index must include an 'is_deleted' field: "
                    "{}".format(self.search_class.__class__.__name__)
                )

    def _check_query(self, bool_data):
        """Check the query - are we able to handle soft deletes?

        .. note:: This method will fail if the ``bool`` section contains a
                  ``filter`` or ``minimum_should_match``.  Feel free to update
                  the code to handle this scenario if required.

        """
        if "filter" in bool_data or "minimum_should_match" in bool_data:
            raise SearchError(
                "Don't know how to update the query because a 'filter' "
                "or 'minimum_should_match' section already exists."
            )

    def _create(self, es):
        """Create ElasticSearch index."""
        self._check_configuration()
        es.indices.create(self._index_name(), self.search_class.configuration)
        es.cluster.health(wait_for_status="yellow")

    def _delete(self, es):
        """Delete ElasticSearch index."""
        es.indices.delete(self._index_name())
        es.cluster.health(wait_for_status="yellow")

    def _elasticsearch(self):
        return Elasticsearch(
            [
                {
                    "host": settings.ELASTICSEARCH_HOST,
                    "port": settings.ELASTICSEARCH_PORT,
                }
            ]
        )

    def _explain(self, query, qs):
        """Write the explanation of the score to an ``elastic-explain`` file."""
        file_name = "elastic-explain-{}.json".format(
            timezone.now().strftime("%Y-%m-%d-%H-%M-%S")
        )
        with open(file_name, "w") as f:
            f.write(
                "ElasticSearch Explain - {}".format(
                    timezone.now().strftime("%d/%m/%Y %H:%M")
                )
            )
            f.write("\nIndex: {}\n".format(self._index_name()))
            f.write("\nQuery:\n")
            f.write(json.dumps(query, indent=4))
            f.write("\nResults:\n")
            for hit in qs["hits"]["hits"]:
                f.write("{}".format("_" * 80))
                pk = int(hit["_id"])
                score = hit["_score"]
                f.write("\npk: {}  score: {}\n".format(pk, score))
                f.write(json.dumps(hit["_explanation"]["details"], indent=4))
                f.write("\n")

    def _index_name(self):
        return "{}_{}".format(
            settings.DOMAIN.replace(".", "_"), self.search_class.index_name
        )

    def _query(self, criteria, include_deleted):
        """Exclude deleted records if the class handles a soft delete.

        Using a ``bool`` field with ``term`` ``filter`` assigns a score of 0
        to contacts which only match the 'is_deleted' field:
        https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-bool-query.html#_scoring_with_literal_bool_filter_literal

        I am not sure how ``minimum_should_match`` works, but it seems to
        exclude contacts which only match the ``is_deleted`` flag.

        """
        query = self.search_class.query(criteria)
        if self.search_class.is_soft_delete:
            if not include_deleted:
                bool_data = query["query"]["bool"]
                self._check_query(bool_data)
                bool_data.update(
                    {
                        "filter": {"term": {"is_deleted": False}},
                        "minimum_should_match": 1,
                    }
                )
        return query

    def analyze(self, analyzer, text):
        """Analyze the text using 'analyzer' to see how it is being tokenized.

        .. note:: This method is for debugging/understanding what is happening.

        Example::

          import json
          from contact.search import ContactIndex
          from search.search import SearchIndex

          index = SearchIndex(ContactIndex())
          index.drop_create()
          result = index.analyze('autocomplete', 'EX2 2AB')
          print(json.dumps(result, indent=4))

        .. note:: ``autocomplete`` is an analyzer defined in ``ContactIndex``.

        """
        es = self._elasticsearch()
        return es.indices.analyze(
            self._index_name(), {"analyzer": analyzer, "text": text}
        )

    def count(self):
        es = self._elasticsearch()
        data = es.count(self._index_name())
        return data.get("count")

    def drop_create(self):
        """Create the Elasticsearch index."""
        es = self._elasticsearch()
        try:
            self._delete(es)
        except NotFoundError as e:
            # not found is not a problem - the next step is to create it
            pass
        self._create(es)

    def _parse(self, pk):
        """Iterate through the queryset and index all the rows."""
        qs = self.search_class.queryset(pk)
        for row in qs:
            op_type = "index"
            is_deleted = row.is_deleted
            source = self.search_class.source(row)
            if self.search_class.is_soft_delete:
                source.update({"is_deleted": is_deleted})
            elif is_deleted:
                op_type = "delete"
            yield {
                "_op_type": op_type,
                "_id": row.pk,
                "_index": self._index_name(),
                "_type": self.search_class.document_type,
                "_source": source,
            }

    def rebuild(self):
        self.drop_create()
        return self.update()

    def refresh(self):
        """Rebuild the index without dropping first."""
        return self.update()

    def search(
        self,
        criteria,
        data_format=None,
        include_deleted=None,
        page_number=None,
        page_size=None,
        explain=None,
    ):
        """Search the index.

        1. Return the results (``list`` of ``Hit`` objects).
        2. Return the ``total`` number of documents matching the ``criteria``.

        If searching the ? index, then match on the ``description`` field
        as well as the part number.

        Multiple Query Strings:
        https://www.elastic.co/guide/en/elasticsearch/guide/current/multi-query-strings.html

        Query-Time Boosting:
        https://www.elastic.co/guide/en/elasticsearch/guide/current/query-time-boosting.html

        Sort:
        https://www.elastic.co/guide/en/elasticsearch/reference/current/search-request-sort.html

        """
        result = []
        total = 0
        if data_format is None:
            data_format = SearchFormat.ATTR
        if explain is None:
            explain = False
        if page_size is None:
            page_size = self.PAGE_SIZE
        if criteria:
            es = self._elasticsearch()
            query = self._query(criteria, include_deleted)
            if page_number and page_number > 1:
                from_row = (page_number - 1) * page_size
                if (from_row + page_size) > self.MAX_RESULT_WINDOW:
                    raise SearchError(
                        "The maximum result window for searching is {}. "
                        "Your query is for {} ('from_row' {}, "
                        "'page_number' {})".format(
                            self.MAX_RESULT_WINDOW,
                            from_row,
                            page_number,
                            page_size,
                        )
                    )
            else:
                from_row = 0
            qs = es.search(
                index=self._index_name(),
                body=query,
                from_=from_row,
                size=page_size,
                explain=explain,
            )
            total = qs["hits"]["total"]
            for hit in qs["hits"]["hits"]:
                pk = int(hit["_id"])
                document_type = hit["_type"]
                score = hit["_score"]
                source = hit["_source"]
                is_deleted = source.get("is_deleted")
                if data_format == SearchFormat.ATTR:
                    data = self.search_class.data_as_attr(source, pk)
                elif data_format == SearchFormat.COLUMN:
                    data = self.search_class.data_as_fmt(source, pk)
                else:
                    raise SearchError(
                        "The 'data_format' must be either '{}' or "
                        "'{}'".format(SearchFormat.ATTR, SearchFormat.COLUMN)
                    )
                result.append(
                    Hit(
                        pk=pk,
                        document_type=document_type,
                        score=score,
                        is_deleted=is_deleted,
                        data=data,
                    )
                )
            if explain and settings.DEBUG:
                self._explain(query, qs)
        return result, total

    def update(self, pk=None):
        """Update the ElasticSearch index.

        Keyword arguments:
        update_recent_only -- only update recently modified parts (default None)

        Streaming code copied from:
        https://github.com/elastic/elasticsearch-py/blob/master/example/load.py

        - I am not sure what to think of the code here.  I am deleting documents
          even if they were never added to the index, so I have to tell the system
          not to raise an error (``raise_on_error=False``).
        - For the index refresh (running every five minutes) it would be good if we
          could find the parts which have recently changed to zero stock available,
          then we could just delete those items.

        """
        count = 0
        es = self._elasticsearch()
        for ok, result in streaming_bulk(
            es, self._parse(pk), raise_on_error=False
        ):
            action, state = result.popitem()
            # process the information from ES whether the document has been
            # successfully indexed
            if not ok:
                status = state["status"]
                x = state.get("result")
                found = True
                if x == "found":
                    pass
                elif x == "not_found":
                    found = False
                else:
                    logger.error(
                        "Unknown (or missing) 'state' '{}' '{}'".format(
                            x, state
                        )
                    )
                # not an error to try and delete a document which does not exist
                if action == "delete" and status == 404 and not found:
                    pass
                else:
                    doc_id = state["_id"]
                    logger.error(
                        "Failed to {} document '{}': {}".format(
                            action, doc_id, state
                        )
                    )
            else:
                count = count + 1
        es.indices.refresh(self._index_name())
        return count
