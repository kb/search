# -*- encoding: utf-8 -*-
import importlib
import json

from django.core.management.base import BaseCommand

from search.search import SearchIndex


class Command(BaseCommand):

    help = "Display index count"

    def _index_class(self, x):
        pos = x.rfind(".")
        module_name = x[:pos]
        index_module = importlib.import_module(module_name)
        class_name = x[pos + 1 :]
        index_class = getattr(index_module, class_name)
        self.stdout.write("Found '{}' class...".format(index_class.__name__))
        return index_class

    def add_arguments(self, parser):
        parser.add_argument(
            "index_class_name",
            nargs="+",
            type=str,
            help="Class name of the index e.g. 'contact.search.ContactIndex'",
        )

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        names = options["index_class_name"]
        for counter, name in enumerate(names):
            index_class = self._index_class(name)
            index = SearchIndex(index_class())
            count = index.count()
            self.stdout.write(
                "{}. {} {}".format(
                    counter + 1,
                    index_class.__name__,
                    json.dumps(count, indent=4),
                )
            )
        self.stdout.write("{} - Complete".format(self.help))
