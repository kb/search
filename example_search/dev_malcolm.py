# -*- encoding: utf-8 -*-
import socket
from .base import *


DATABASE = "dev_app_search_malcolm"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": DATABASE,
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    }
}


MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)

INSTALLED_APPS += (
    # 'django.contrib.formtools',
    "django_extensions",
    "debug_toolbar",
)


# simple engine (dev only)
HAYSTACK_CONNECTIONS = {
    "default": {"ENGINE": "haystack.backends.simple_backend.SimpleEngine"}
}

# HAYSTACK_CONNECTIONS = {
#    'default': {
#        'BATCH_SIZE': 100,
#        'ENGINE': (
#            'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine'
#         ),
#        'INDEX_NAME': 'example_search',
#        'TIMEOUT': 60 * 5,
#        'URL': 'http://127.0.0.1:9200/',
#    },
# }

ALLOWED_HOSTS = [
    "127.0.0.1",
    "127.0.1.1",
    "localhost",
    socket.gethostbyname(socket.gethostname()),
]

INTERNAL_IPS = ["127.0.0.1"]
