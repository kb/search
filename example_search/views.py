# -*- encoding: utf-8 -*-
from django.views.generic import TemplateView

from base.view_utils import BaseMixin
from contact.search import ContactIndex
from example_search.search import TicketIndex
from search.views import SearchViewMixin


class HomeView(BaseMixin, TemplateView):

    template_name = "example/home.html"


class SearchMultiView(SearchViewMixin):

    INDEX_CHOICES = (("ticket", "Ticket"), ("contact", "Contact"))
    INDEX_CLASSES = {"contact": ContactIndex, "ticket": TicketIndex}


class SearchSingleView(SearchViewMixin):
    """This view has one choice for 'INDEX_CHOICES'."""

    paginate_by_for_search = 3
    INDEX_CHOICES = (("contact", "Contact"),)
    INDEX_CLASSES = {"contact": ContactIndex}


class SettingsView(BaseMixin, TemplateView):

    template_name = "example/settings.html"
