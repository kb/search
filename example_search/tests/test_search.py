# -*- encoding: utf-8 -*-
import pytest

from contact.search import ContactIndex
from contact.tests.factories import ContactEmailFactory, ContactFactory
from search.models import SearchFormat
from search.search import SearchError, SearchIndex


class InvalidConfigIndex:
    @property
    def configuration(self):
        return {
            "mappings": {
                self.document_type: {
                    "properties": {
                        "email": {"type": "text", "analyzer": "email"}
                    }
                }
            }
        }

    @property
    def document_type(self):
        """Elasticsearch document type."""
        return "contact"

    @property
    def index_name(self):
        """Name of the Elasticsearch index (will have the domain added)."""
        return "contact"

    @property
    def is_soft_delete(self):
        return True


@pytest.mark.elasticsearch
def test_configuration():
    index = SearchIndex(InvalidConfigIndex())
    with pytest.raises(SearchError) as e:
        index.drop_create()
    assert (
        "The 'configuration' for a soft delete index "
        "must include an 'is_deleted' field"
    ) in str(e.value)


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_count():
    c1 = ContactFactory()
    ContactEmailFactory(contact=c1, email="patrick@kbsoftware.co.uk")
    c2 = ContactFactory()
    ContactEmailFactory(contact=c2, email="web@pkimber.net")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 0 == index.count()
    assert 2 == index.rebuild()
    assert 2 == index.count()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_refresh():
    c1 = ContactFactory()
    ContactEmailFactory(contact=c1, email="patrick@kbsoftware.co.uk")
    c2 = ContactFactory()
    ContactEmailFactory(contact=c2, email="web@pkimber.net")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 0 == index.count()
    assert 2 == index.refresh()
    assert 2 == index.count()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_email():
    """Copied from 'contact/tests/test_search.py'."""
    c1 = ContactFactory()
    ContactEmailFactory(contact=c1, email="patrick@kbsoftware.co.uk")
    c2 = ContactFactory()
    ContactEmailFactory(contact=c2, email="web@pkimber.net")
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 2 == index.update()
    result, total = index.search(
        "web@pkimber.net", data_format=SearchFormat.COLUMN
    )
    assert 1 == total
    assert [c2.pk] == [x.pk for x in result]
    assert ["contact"] == [x.document_type for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_max_result_window():
    index = SearchIndex(ContactIndex())
    index.drop_create()
    assert 0 == index.update()
    with pytest.raises(SearchError) as e:
        index.search(
            "web@pkimber.net",
            data_format=SearchFormat.COLUMN,
            page_number=9999,
            page_size=9,
        )
    assert (
        "The maximum result window for searching is 10000. "
        "Your query is for 89982 ('from_row' 9999, 'page_number' 9)"
    ) in str(e.value)
