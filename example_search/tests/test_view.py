# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from base.url_utils import url_with_querystring
from contact.search import ContactIndex
from contact.tests.factories import ContactFactory
from login.tests.factories import UserFactory
from search.search import SearchIndex


def _create_contact_index():
    ContactFactory(
        user=UserFactory(
            username="apple", first_name="Orange", last_name="Pippin"
        )
    )
    ContactFactory(
        user=UserFactory(
            username="pear", first_name="Apple", last_name="Crumble"
        )
    )
    index = SearchIndex(ContactIndex())
    index.rebuild()


@pytest.mark.django_db
def test_search_multi(client):
    response = client.get(reverse("project.search"))
    assert 200 == response.status_code, response
    assert "criteria" in response.context
    # I don't think we need the 'criteria' to be None, but it is...
    assert response.context["criteria"] is None
    assert "form" in response.context
    form = response.context["form"]
    assert "criteria" in form.fields
    assert "index" in form.fields
    assert [("ticket", "Ticket"), ("contact", "Contact")] == form.fields[
        "index"
    ].choices


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_multi_params(client):
    _create_contact_index()
    url = url_with_querystring(
        reverse("project.search"), criteria="apple", index="contact"
    )
    response = client.get(url)
    assert 200 == response.status_code, response
    # form
    assert "form" in response.context
    form = response.context["form"]
    assert "criteria" in form.fields
    assert "index" in form.fields
    assert [("ticket", "Ticket"), ("contact", "Contact")] == form.fields[
        "index"
    ].choices
    # url
    assert "criteria" in response.context
    assert "apple" in response.context["criteria"]
    assert "index_name" in response.context
    assert "contact" in response.context["index_name"]
    # queryset
    assert "object_list" in response.context
    object_list = response.context["object_list"]
    assert 2 == len(object_list)


@pytest.mark.django_db
def test_search_single(client):
    """This view has a single index choice, so the user cannot choose."""
    response = client.get(reverse("search.single"))
    assert 200 == response.status_code, response
    assert "form" in response.context
    form = response.context["form"]
    assert "criteria" in form.fields
    assert "index" not in form.fields


@pytest.mark.django_db
@pytest.mark.elasticsearch
@pytest.mark.parametrize(
    "page_number,expect",
    [
        ("", ["A1 Apple", "A2 Apple", "A3 Apple"]),
        ("ABC", ["A1 Apple", "A2 Apple", "A3 Apple"]),
        (0, ["A1 Apple", "A2 Apple", "A3 Apple"]),
        (1, ["A1 Apple", "A2 Apple", "A3 Apple"]),
        (2, ["A4 Apple", "A5 Apple", "A6 Apple"]),
        (3, ["A7 Apple"]),
        (4, []),
        (99, []),
    ],
)
def test_search_single_criteria_pagination(client, page_number, expect):
    """Test the search pagination (uses ``ElasticPaginator``).

    .. note:: I create 7 'Apple' rows and 7 'Orange' rows.  I then search for
              ``apple`` and make sure the pagination works properly.

    """
    for name in ("Apple", "Orange"):
        for x in range(1, 8):
            ContactFactory(
                company_name="",
                user=UserFactory(
                    username="{}-{}".format(name.lower(), x),
                    first_name="A{}".format(x),
                    last_name=name,
                ),
            )
    index = SearchIndex(ContactIndex())
    index.rebuild()
    url = url_with_querystring(
        reverse("search.single"),
        criteria="apple",
        index="contact",
        page=page_number,
    )
    response = client.get(url)
    assert 200 == response.status_code, response
    assert "object_list" in response.context
    object_list = response.context["object_list"]
    assert expect == [x.data[0][0].text for x in object_list]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_search_single_params(client):
    _create_contact_index()
    # The 'index' parameter will not be created by the form because
    # ``example_search.views.SearchSingleView`` has only one index choice
    url = url_with_querystring(reverse("search.single"), criteria="apple")
    response = client.get(url)
    assert 200 == response.status_code, response
    # form
    assert "form" in response.context
    form = response.context["form"]
    assert "criteria" in form.fields
    assert "index" not in form.fields
    # url
    assert "criteria" in response.context
    assert "apple" in response.context["criteria"]
    assert "index_name" in response.context
    assert "contact" in response.context["index_name"]
    # queryset
    assert "object_list" in response.context
    object_list = response.context["object_list"]
    assert 2 == len(object_list)
