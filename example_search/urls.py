# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path, re_path, reverse_lazy
from django.views.generic import RedirectView

from .views import HomeView, SearchMultiView, SearchSingleView, SettingsView


urlpatterns = [
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(
        r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    re_path(r"^", view=include("login.urls")),
    # re_path(r'^contact/',
    #     view=include('contact.urls')
    #     ),
    re_path(
        r"^contact/(?P<pk>\d+)/$",
        view=RedirectView.as_view(url=reverse_lazy("project.home")),
        name="contact.detail",
    ),
    re_path(
        r"^search/multi/$",
        view=SearchMultiView.as_view(),
        name="project.search",
    ),
    re_path(
        r"^search/single/$",
        view=SearchSingleView.as_view(),
        name="search.single",
    ),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(url=reverse_lazy("project.home")),
        name="project.dash",
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

# urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [re_path(r"^__debug__/", include(debug_toolbar.urls))]
