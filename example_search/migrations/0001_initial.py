# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-23 15:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = []

    operations = [
        migrations.CreateModel(
            name="Cake",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=100)),
                ("description", models.TextField()),
                ("deleted", models.BooleanField(default=False)),
            ],
            options={"verbose_name": "Cake", "verbose_name_plural": "Cakes"},
        ),
        migrations.CreateModel(
            name="Coffee",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=100)),
                ("rating", models.IntegerField()),
                ("deleted", models.BooleanField(default=False)),
            ],
            options={
                "verbose_name": "Coffee",
                "verbose_name_plural": "Coffees",
            },
        ),
    ]
