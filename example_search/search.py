# -*- encoding: utf-8 -*-
from example_search.models import Coffee
from search.search import Fmt


class TicketIndexMixin:
    """This is (probably) a non-functional test index."""

    def _settings(self):
        """Index settings."""
        return {
            "number_of_shards": 1,
            "analysis": {
                "analyzer": {
                    "autocomplete": {
                        "tokenizer": "autocomplete",
                        "filter": ["lowercase"],
                    }
                }
            },
        }

    @property
    def configuration(self):
        """Index configuration.  Used by the Elasticsearch 'create' method."""
        return {
            "mappings": {
                self.document_type: {
                    "properties": {
                        "name": {"type": "text", "analyzer": "english"}
                    }
                }
            },
            "settings": {"index": self._settings()},
        }

    def data(self, source, pk):
        """A row of data returned as part of the search results."""
        return [[Fmt("{name}".format(**source), url=True)]]

    @property
    def document_type(self):
        """Elasticsearch document type."""
        return "ticket"

    @property
    def index_name(self):
        """Name of the Elasticsearch index (will have the domain added)."""
        return "ticket"

    def query(self, criteria):
        """The Elasticsearch query."""
        should = [{"match": {"name": criteria}}]
        return {"query": {"bool": {"should": should}}}

    def queryset(self, pk):
        """Build the Elasticsearch index using this queryset."""
        if pk is None:
            qs = Coffee.objects.all().order_by("name")
        else:
            qs = Coffee.objects.filter(pk=pk)
        return qs

    def source(self, row):
        """Build the index using this data from the 'queryset'.

        .. note:: The full name includes the company name
                  (see the ``Contact`` model for more information).

        """
        return {"name": row.name}

    def table_headings(self):
        """Table headings for the results."""
        return ["Name"]

    def url_name(self):
        """URL name for the search results."""
        return "contact.detail"


class TicketIndex(TicketIndexMixin):
    @property
    def is_soft_delete(self):
        return False
