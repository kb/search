bleach==4.1.0
django-braces==1.15.0
django-dramatiq==0.11.0
django-extensions==3.1.5
django-reversion==5.0.2
django-taggit==3.0.0
Django==4.0.8
dramatiq[redis, watch]==1.13.0
elasticsearch==6.8.1
humanize==4.1.0
psycopg2==2.9.3
pytz==2022.4
