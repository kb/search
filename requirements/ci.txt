-r base.txt
-e .
-e git+https://gitlab.com/kb/base.git#egg=kb-base
-e git+https://gitlab.com/kb/contact.git#egg=kb-contact
-e git+https://gitlab.com/kb/login.git#egg=kb-login
-e git+https://gitlab.com/kb/mail.git#egg=kb-mail
django-debug-toolbar
factory-boy
pytest-cov
pytest-django
pytest-flakes
pytest-pep8
